Introduction
====

A video card generates a feed of output images to a display

Can be discrete or dedicated

Has graphics processing unit at the core

--------------
GPU
====
- Originally designed for the rendering of triangles in 3D graphics
- handle billions of repetitive low level tasks, compared to CPUs which are designed to handle multiple different complex tasks
- specialized for compute-intensive, highly parallel computation
- more transistors are devoted to data processing rather than data caching and flow control
- suited to data-parallel computations: the same program is executed on many data elements in parallel with a high ratio of arithmetic operations to memory operations
- has thousands of arithmetic logic units (ALUs) compared with traditional CPUs that commonly have only 4 or 8
- the same program is executed for each data element
- lower requirement for sophisticated flow control
- same program executed on many data elements and has high arithmetic intensity, the memory access latency can be hidden with calculations instead of big data caches.
- applications that process large data sets can use a data-parallel programming model to speed up the computations
- In 3D rendering, large sets of pixels and vertices are mapped to parallel threads.
- Similarly, image and media processing applications such as post-processing of 
rendered images, video encoding and decoding, image scaling, stereo vision, and pattern recognition can map image blocks and pixels to 
parallel processing threads.
- In fact, many algorithms outside the field of image rendering and processing are accelerated by data-parallel processing, from general signal processing or physics simulation to computational finance or computational biology.
- The advent of multicore CPUs and manycore GPUs means that mainstream processor chips are now parallel systems.
- Their parallelism continues to scale with Moore's law. The challenge is to develop application software that transparently scales its parallelism to leverage the increasing number of processor cores, much as 3D graphics applications transparently scale their parallelism to multicore GPUs with widely varying numbers of cores.
- When computer scientists first attempted to use GPUs for scientific computing, the scientific codes had to be mapped onto the matrix operations for 
manipulating traingles. This was incredibly difficult to do, and took a lot of time and dedication.
- there are now high level languages (such as CUDA and OpenCL) that target the GPUs directly
- A GPU program comprises two parts: a *host* part the runs on the CPU and one or more *kernels* that run on the GPU. 
- CPU portion of the program is used to set up the parameters and data for the computation, while the kernel portion performs the actual computation.
- In some cases the CPU portion may comprise a parallel program that performs message passing operations using MPI.

**Jargon**
- Transistor:  semiconductor device used to amplify or switch electronic signals
- Flow control: flow control is the process of managing the rate of data transmission between two nodes to prevent a fast sender from overwhelming a slow receiver
- arithmetic logic unit: electronic circuit that performs arithmetic and bitwise operations on integer binary numbers
- Moore's law: observation that the number of transistors in a dense integrated circuit doubles about every two years
- threads:  a thread of execution is the smallest sequence of programmed instructions that can be managed independently by a scheduler
- scheduler: The scheduler is an operating system module that selects the next jobs to be admitted into the system and the next process to run
- Parallel computation: Type of computation in which many calculations or the execution of processes are carried out simultaneously. Problems can often be divided into smaller ones, which can then be solved at the same time.
- FLOPS: floating point operations per second
----------------
OpenGL
=======
[wikipedia]
- OpenGL describes an abstract API for rendering 2D and 3D graphics
- API is used to interact with a graphics processing unit (GPU) to achieve hardware-accelerated rendering.
- possible for the API to be implemented entirely in software
- designed to be implemented mostly or entirely in hardware.
- API is defined as a set of functions which may be called by the client program, alongside a set of named integer constants
- has many language bindings
- Important bindings: Javascript/WebGL, C/WGL (Windows), C/GLX (X Window System)
![](https://upload.wikimedia.org/wikipedia/commons/d/da/Pipeline_OpenGL.svg)

[khronos.org](https://www.khronos.org/opengl/wiki/Portal:OpenGL_Concepts)
- specification that describes the behavior of a rasterization-based rendering system
- defines the API through which a client application can control this systems
- rendering system is carefully specified to make hardware implementations allowable
- hardware vendor implementations of OpenGL are called "drivers"
- drivers translate API commands into GPU instructions
- exposed to software as an API written against  with bindings for other languages

OpenGL context:
- context contains all of the information that will be used by the OpenGL system to render, when the system is given a rendering command
- OpenGL cannot be used without a context
- contexts are local to a particular application
- each application can have multiple contexts
- Each context is completely separate and isolated from one another
- objects can be shared in between contexts
- All OpenGL functions operate on the context which is "current". like a global variable that every OpenGL function operates on. 
- OpenGL specification only details the behavior of contexts
- creation of contexts is governed by platform-specific APIs
- OpenGL code is generally platform-neutral, it is always dependent on some platform-specific code to create and destroy OpenGL contexts

OpenGL state:

- OpenGL context contains information used by the rendering system. This information is called State
- A piece of state is simply some value stored in the OpenGL context.
- Every individual piece of state in the OpenGL context can be uniquely identified. 
- When a context is created, every piece of state is initialized to a well-defined default value
- OpenGL functions can be grouped into 3 broad categories: functions that set state into the context, functions that query state, and functions that render given the current state of the context.

OpenGL commands and sync:
- implementations attempt to do as much work asynchronously as possible
- When a rendering function returns, odds are good that the actual rendering operation has not even started yet, let alone has finished. 
- as long as you don't query any state that would be changed by that rendering command (such as the content of the destination images)
- The implementation must make rendering look synchronous, while being as asynchronous as possible. 
- avoid querying state that has been affected by rendering commands. 
- avoid changing the contents of objects that will be used by rendering commands that have been sent but may not yet have executed

OpenGL object model:
- OpenGL state values are aggregated into objects. 
- allows groups of state to be stored and reset with a single command. 
- objects in OpenGL are defined as groups of context state
- The object 0 is special, and it has different behaviors for different object types 
- For many objects, 0 represents "not an object", much like a NULL pointer
- For a few object types, it is a default object, which can not be destroyed
- In general, with the exception of Framebuffers, you should treat 0 like NULL: an invalid object.

OpenGL memory model:
- governs when state set by one OpenGL command becomes visible to other commands
- memory model is perfectly sequential and synchronous: state set by one command is immediately visible to any subsequent command, but not any prior ones
- there are certain cases where the model breaks down or otherwise requires explicit synchronization
- rendering to the same image as one is sampling from is one such case
- A second case is when dealing with multiple contexts that have objects shared between them
- State set from one context may not be immediately visible in another; special considerations need to be taken
- The big one has to do with arbitrary image reads/writes in GL 4.2+
- The use of this feature (as well as some related ones) completely throws out any implicit GL memory model, thus forcing all synchronization for these operations on the user.

OpenGL rendering pipeline:
- Rendering Pipeline is the sequence of steps that are taken by the OpenGL rendering system when the user issues a Drawing Command
![](https://upload.wikimedia.org/wikipedia/commons/d/da/Pipeline_OpenGL.svg)
- pipeline is a sequential series of steps, where each step performs some computation and passes its data to the next step for further processing
- specification is very clear on the order in which objects are rendered
- objects are rendered in the exact order the user provides
- implementations are free to adjust order internally
- rendered output must be as if it were processed in order

OpenGL shaders:
- some stages of the rendering pipeline are programmable
- The term for a programmable stage of the pipeline is Shader, and the programs that are executed by that pipeline stage are called "Shaders"
- In OpenGL, shaders are written in the OpenGL Shading Language
- Many OpenGL implementations offer extensions that provide alternative languages.

OpenGL framebuffer:
- Framebuffer is a OpenGL object that aggregates images together, which rendering operations can use as their eventual destination. 
- OpenGL contexts usually have a Default Framebuffer, which is usually associated with a window or other display device. Rendering operations that are intended to be directly visualized should go to the default framebuffer.
- Users can construct Framebuffer Objects, framebuffers built from images provided by Textures or Renderbuffers.
- These are useful for rendering data off-screen; the data can later be transferred to the default framebuffer.

**Jargon**
rasterization: task of taking an image described in a vector graphics format (shapes) and converting it into a raster image (pixels or dots)

hardware-accelerated: use of computer hardware specially made to perform some functions more efficiently than is possible in software running on a general-purpose CPU. 

rendering: process of generating an image from a 2D or 3D model

language binding: bindings are wrapper libraries that bridge two programming languages, so that a library written for one language can be used in another language

----------------
3DFX Voodoo1 
=======
- 50 MHz core/memory clock 
- 4 MB of RAM
- 3D-only card
- incapable of running Windows on its own
- needed to be used with a 2D graphics card

3DFX Glide API
=======
- small implementation of OpenGL
- OpenGL was a big library with a large number of API calls
- many of these API calls were not useful
- Glide selected features only useful for real-time 3d videogame rendering
- created an API which was small, easy to use and implementable entirely in 90s hardware

API opensourced and available at [glide.sourceforge.net](http://glide.sourceforge.net/)

- ~~[Glidos](http://www.glidos.net/install.html): emulator for the Glide API that allows older games to run Glide, even on non-3dfx hardware~~ proprietary & closed source
- [OpenGLide](https://sourceforge.net/projects/openglide/): Glide to OpenGL wrapper
----------------
Mesa
====
- open source software implementation of OpenGL, Vulkan, and other graphics API specifications
- translates specifications to vendor-specific graphics hardware drivers
- most important users are two graphics drivers mostly developed and funded by Intel and AMD
- AMD promotes their Mesa drivers Radeon and RadeonSI over the deprecated AMD Catalyst
- Intel has only supported the Mesa driver
- Proprietary graphics drivers (e.g. Nvidia GeForce driver and Catalyst) replace all of Mesa, providing their own implementation of a graphics API
- open-source effort to write a Mesa Nvidia driver called Nouveau

----------------
X Window System
===============

----------------
History
=======
- ***IBM monochrome display adapter, 1981***
-> Had 4 kb of video memory (VRAM)
-> Each character is rendered in a box of 9×14 pixels,  7×11 depicts the character  (the other pixels used for space between character columns and lines)
-> cannot address individual pixels; it only works in text mode
-> display patterns limited to 256 characters
-> character patterns are stored in ROM
-> used 9 pin DE-9 connector

- ***IBM Color Graphics Adapter, 1981***
-> first graphics card for IBM PC and first color display standard
-> 16kb of VRAM
-> can be connected to CRT monitor through RGBI interface or NTSC TV/composite monitor with RCA connectors
-> Built around the Motorola MC6845
-> featured several graphics and text modes
-> highest display resolution of any mode was 640×200
-> highest color depth supported was 4-bit (16 colors)
-> Supported Windows 3.0
-> used 9 pin DE-9 connector
-> CGA could display only four colors at a time at a screen resolution of 320×200 pixels

- ***Hercules Graphics Card, 1982***
-> offer both high quality text and graphics from a single card
-> Motorola MC6845
-> widely supported de facto display standard on IBM PC compatibles
-> used long after more technically capable systems had entered the market, especially on dual-monitor setups
-> text mode could display 80×25 text characters and was MDA-compatible. Clearer than competing IBM CGA
-> graphics mode made all pixels directly addressable. This translated to a resolution of not 720×350, but only 720×348 pixels (at 1 bit per pixel) 
-> for technical reasons, the screen height had to be a multiple of four
-> supported two graphic pages, one at address B0000h and one at address B8000h
-> second page could be enabled or disabled by software
-> When disabled, the addresses used by the card did not overlap with those used by color adapters such as CGA or VGA. This made dual-screen operation possible simply through installation of a Hercules card next to a VGA or CGA adapter
-> Clone boards offered Hercules compatibility (example: ATI, Yamaha, NEC)
-> Hercules Graphics Card included a diskette with HBASIC, an interpreted version of BASIC that enabled programming graphics on a monochrome monitor
-> In text modes, the memory appears just like an MDA card.
-> Video timing on the HGC is provided by the Motorola 6845 video controller
-> The address of a given screen location in memory is given by the formula: `address = (0xB0000) + ( row × 160 ) + (column × 2)`
-> To realize a graphics mode with 348 scanlines on the HGC, the MC6845 is programmed with 87 character rows per picture and four scanlines per character row.
-> Because the video memory address output by the MC6845 is identical for each row within a character, the HGC must use the MC6845's "row address" output (i.e. the scanline within the character row) as additional address bits to fetch raster data from video memory
-> unless the size of a single scanline's raster data is a power of two, raster data cannot be laid out continuously in video memory.
-> lines are interleaved and thus addressing is slightly more complicated. There are 8 pixels per byte, 90 bytes per line
-> Consecutive lines on the screen are interleaved by 4 lines in memory, so in memory it looks like this:
```
Screen Line #0 starts at B000:0000
Screen Line #1 starts at B000:2000
Screen Line #2 starts at B000:4000
Screen Line #3 starts at B000:6000
Screen Line #4 starts at B000:005A
```
-> The memory address that contains a given pixel is given by:
`mem = (0xb8000) + ((y&3)<<13)+(y>>2)×90+(x>>3)`
-> The lack of preset BIOS modes for graphics meant the card was quite reprogrammable, within the limits of monitor sync tolerance.
-> A somewhat popular modified driver called HERKULES was able to produce 720x360 graphics or 90x45 text (8x8 font) on a standard display

- ***Tandy Graphics Adaptor***
-> TGA system could display up to 16 colors
-> available only integrated onto computer motherboards, not on a separate card
-> Tandy Video 1 Output: 320×200 in 4 colors from a 16 color hardware palette. Pixel aspect ratio of 1:1.2. 640×200 in 2 colors. Pixel aspect ratio of 1:2.4
-> Tandy Video 2 Output: 640x200 with 16 colors
-> The full 16 color CGA palette was available: black, gray, red, light red, brown, yellow, green, light green, blue, light blue, magenta, light magenta, light gray, white
-> TGA uses some of the main RAM as the video RAM. 64 KB or 128 KB of base RAM is special in that it is shared with the PCjr video subsystem.
-> it saves the cost of dedicated video RAM, and the dynamic RAM is refreshed by the 6845 CRT controller as long as the video is running, so there is no need for separate DRAM refresh circuitry
-> The shared-RAM scheme does have some advantages: up to almost 128 KB of RAM can be used for video (if software is mostly in ROM—e.g. on PCjr cartridges—or in RAM above the first 128 KB), and the displayed video banks can be switched instantaneously to implement double-buffering (or triple-buffering, or up to 7-fold buffering in 16 KB video modes) for smooth full-screen animation, something the CGA cannot do.
-> A major advance in the PCjr (and TGA) video subsystem was the addition of a programmable palette which can map each 4-bit video (pixel) value to any of the 16 colors of the CGA palette

- ***IBM Enhanced Graphics Adapter, 1984***
-> 640×350 w/16 colors (from a 6 bit palette of 64 colors), pixel aspect ratio of 1:1.37.
-> Extended to 640×400, 640×480, 720×540 by third party boards
-> EGA palette allows all 16 CGA colors to be used simultaneously, and it allows substitution of each of these colors with any one from a total of 64 colors (two bits each for red, green and blue)
![](https://upload.wikimedia.org/wikipedia/commons/f/fa/Birds_ega.png)
-> uses nine-pin D-subminiature (DE-9) connector which looks identical to the CGA connector. The hardware signal interface, including the pin configuration, is largely compatible with CGA

-> EGA graphics modes are planar 
-> The video memory is divided into four pages (except 640×350×2, which has two pages), one for each component of the RGBI color space.
-> Each bit represents one pixel. If a bit in the red page is enabled, but none of the equivalent bits in the other pages are, a red pixel will appear in that location on-screen.
-> If all the other bits for that particular pixel were also enabled, it would become white, and so forth. 
-> The planes are 8 KB in size (200-line modes and 640×350 with two colors and 32 KB video RAM), 16 KB (640×350 with 64 KB video RAM), or 32 KB (640×350 with 128 KB video RAM) and reside at segment A000 in the CPU's address space.
-> They are bank-switched and only one plane can be read from at once
-> The programmer may set the control registers on the EGA card to select which planes are written to. 
-> It is possible to write to all of them at once even though just one plane can be read from at any given moment.
-> Most software made up to 1991 can run in EGA
-> SimCity is a notable example of a commercial game that runs in 640×350×16 mode.

- ***IBM Video Graphics Array, 1987***
-> graphics standard for video display controller first introduced with the IBM PS/2
-> supports both All Points Addressable graphics modes and alphanumeric text modes.
-> 640×480 in 16 colors or monochrome
-> VGA is referred to as an "Array" instead of an "adapter" because it was implemented from the start as a single chip
-> a single application-specific integrated circuit (ASIC) replaced both the Motorola 6845 video address generator as well as dozens of discrete logic chips that covered the full-length ISA boards of the MDA, CGA, and EGA
-> 256 KB Video RAM
-> 16-color and 256-color paletted display modes.
-> 262,144-color global palette
-> Selectable 25.175 MHz or 28.322 MHz master pixel clock
-> Usual line rate fixed at 31.46875 kHz
-> 800 horizontal pixels max, 600 vertical pixels max
-> Refresh rates at up to 70 Hz
-> Vertical blank interrupt
-> Planar mode: up to 16 colors
-> Packed-pixel mode: 256 colors
-> Hardware smooth scrolling support
-> No Blitter, but supports very fast data transfers via "VGA latch" registers
-> Barrel shifter
-> Split screen support
-> 75 ohm double-terminated impedance (18.7 mA, 13 mW)
-> 0.7 V peak-to-peak
-> can be congfigured to emulate predecessors EGA CGA MDA
-> three-row 15-pin DE-15 connector
-> video memory of the VGA is mapped to the PC's memory via a window in the range between segments 0xA0000 and 0xBFFFF in the PC's real mode address space (A000:0000 and B000:FFFF in segment:offset notation). Typically, these starting segments are:
```
0xA0000 for EGA/VGA graphics modes (64 KB)
0xB0000 for monochrome text mode (32 KB)
0xB8000 for color text mode and CGA-compatible graphics modes (32 KB)
```

Jargon
======
- CRT: cathode ray tube
- Composite video: analog video transmission that carries standard definition video typically at 480i or 576i resolution as a single channel
- RGBI: red green blue intensity interface for IBM
- NTSC: analog television color system that was used in North America from 1954
- ROM: read-only memory
- Vsync: vertical synchronization
- Bank-switching: way to increase the amount of usable memory beyond the amount directly addressable by the processor. by extending the address bus of a processor with some external register.
- Video display controller: an integrated circuit which is the main component in a video signal generator
- Video signal generator: outputs predetermined video and/or television oscillation waveforms and other signals used in the synchronization of television devices

- -----
- Video memory/VRAM: Dual-ported RAM used to store framebuffer in computer graphics
- computer graphics: pictures created using computers
- Dual-ported RAM: Type of RAM (dynamic RAM or DRAM) used that allows multiple reads or writes at the same time.
- Single-ported RAM: Only allows one/read write at a time
- Dynamic RAM (DRAM): random access semiconductor memory, stores each bit of data in a separate tiny capacitor within an integrated circuit.
- Static RAM (SRAM): semiconductor memory that uses bistable latching circuitry (flip-flop) to store each bit
- flip-flop: a flip-flop or latch is a circuit that has two stable states and can be used to store state information
- Capacitor: passive two-terminal electrical component that stores potential energy in an electric field
- Framebuffer: portion of RAM containing a bitmap that drives a video display
- RAM: form of computer data storage that stores data and machine code currently being used
- bitmap: a bitmap is a mapping from some domain to bits. also called bit array.
- pixmap: a map of pixels, where each one may store more than two colors
- Computer display standards: combination of aspect ratio, display size, display resolution, color depth and refresh rate.
- aspect ratio: ratio of sizes of dimensions of geometric shapes. for rectangles, it is `WIDTH/HEIGHT`
- display size: physical size of the display
- display resolution: number of distinct pixels in each dimension that can be displayed
- pixel: physical point in a raster image. smallest controllable element of a picture represented on the screen.
- color depth: number of bits used to indicate the color of a single pixel
- refresh rate: number of times in a second that a display hardware updates its buffer
- scanline: one line in a raster scanning pattern
- raster scanning: rectangular pattern of image capture and reconstruction in television
- raster graphics: raster graphics or bitmap image is a dot matrix data structure that represents a generally rectangular grid of pixels
- dot matrix:  2-dimensional patterned array, used to represent characters, symbols and images
- interleaved: Interleaving divides memory into small chunks. It is used as a high-level technique to solve memory issues for motherboards and chips

sources
======
1. [Wikipedia](https://wikipedia.org/) CC-BY-SA 3.0
2. https://github.com/nyu-cds/python-gpu/ (C) [Software Carpentry](https://software-carpentry.org/) CC-BY 4.0
3. [OpenGL Wiki](https://www.khronos.org/opengl/wiki/Main_Page)

notes
====
- I felt sleepy after Hercules, so started shortening the subsequent topics. These should be rewritten with proper detail when possible.
- I'm extremely sleepy now so instead of typing and editing I'll just be copying things off verbatim
- Hardware information that is not relevant to the GPU project should be considered for deletion
