# gen2gpu: A RISC-V open source hardware FPGA based 3D accelerator

At the software level, we need to build a kernel DRM driver:

![alt text](https://gitlab.com/gen2gpu/gen2gpu/raw/master/1200px-Linux_kernel_and_OpenGL_video_games.svg.png)

# Some info #
- FPGA board won't output directly to the screen, we'll use an existing card for that.
- We can implement Vulkan and use this to provide OpenGL https://github.com/g-truc/glo
- We should aim to reach the point at which accel is faster than software rendering, and use the cheapest FPGA dev board necessary to do that.
- So basically we are aiming for early 2000s performance in off the shelf cards. (3dfx Voodoo1?)

# Roadmap #
- Read about DRM and try to understand its architecture
- Connect a virtual machine to a vanilla RISC-V emulator through the network port
- Develop RISC-V based drivers for DRM
- Grab an open source RISC-V core
- Develop the tooling to connect the VM to an FPGA emulator on the host
- Make the VM work with said FPGA RISC-V core running on the FPGA emulator
- Transfer the RISC-V core to a real FPGA and make it work with the VM through a network port

# Possibly useful links #

## RISC-V ##
- [Cheat sheet](https://www.cl.cam.ac.uk/teaching/1617/ECAD+Arch/files/docs/RISCVGreenCardv8-20151013.pdf)
- [Short-ish manual](https://github.com/riscv/riscv-asm-manual/blob/master/riscv-asm.md)
- [Long manual](https://content.riscv.org/wp-content/uploads/2017/05/riscv-spec-v2.2.pdf)

## Direct Rendering Manager ##
* [Linux GPU Driver Developer’s Guide](https://static.lwn.net/kerneldoc/gpu/index.html)
* https://github.com/torvalds/linux/blob/master/drivers/gpu/drm/
    * For instance https://github.com/torvalds/linux/blob/master/drivers/gpu/drm/amd/amdgpu/amdgpu_drv.c
* [Kernel Recipes 2017 - An introduction to the Linux DRM subsystem - Maxime Ripard](https://www.youtube.com/watch?v=LbDOCJcDRoo)
* [Anatomy of an Atomic KMS Driver - Laurent Pinchart](https://www.youtube.com/watch?v=5uHMpjz68HE)

## Xorg/Mesa/DRM/Vulkan documentation ##
* [Gallium documentation, useful for figuring out llvmpipe](https://media.readthedocs.org/pdf/gallium/latest/gallium.pdf)
* [History of OpenGL](https://www.khronos.org/opengl/wiki/History_of_OpenGL) (for determining best version to target)
* [Xorg developer's documentation](https://www.x.org/wiki/Development/)

## RISC-V cores ##
- https://github.com/ucb-bar/riscv-mini
- https://github.com/freechipsproject/rocket-chip
- https://github.com/cliffordwolf/picorv32
- https://github.com/skordal/potato

## RISC-V SIMD/vector support ##
* [RISC-V - Packed SIMD (Wikipedia)](https://en.wikipedia.org/wiki/RISC-V#Packed_SIMD)
* https://riscv.org/wp-content/uploads/2015/06/riscv-vector-workshop-june2015.pdf
* [llvm-dev RFC: Supporting the RISC-V vector extension in LLVM](https://lists.llvm.org/pipermail/llvm-dev/2018-April/122517.html)
* [???](http://hwacha.org/)

## GPU Virtualization / llvmpipe ##
- [Virgil: A virtual 3D GPU for qemu](https://www.youtube.com/watch?v=rPeMrmeLTig)
- [VirtualGL](https://wiki.archlinux.org/index.php/VirtualGL)
- [No Vulkan support on VirtualGL](https://github.com/VirtualGL/virtualgl/issues/37)
- https://lists.llvm.org/pipermail/llvm-dev/2018-August/125102.html

## Existing open source GPU implementations ##
* [AMD's HD 7000 series based GPU](https://github.com/VerticalResearchGroup/miaow)
* http://libre-riscv.org/3d_gpu/
* https://ieeexplore.ieee.org/document/7009761
* https://www.youtube.com/watch?v=csvPY6mgN2s
* https://www.youtube.com/watch?v=fqUiLWIIPP0
* https://www.youtube.com/watch?v=cns-uzHPYV0
* https://stackoverflow.com/questions/2218576/using-opengl-es-on-fpga-xilinx
* https://www.quora.com/How-do-I-get-started-on-designing-a-GPU-on-an-FPGA
* http://ece545.com/F16/reports/F09_OpenGL.pdf
* https://hackaday.com/2013/10/11/an-open-source-gpu/

## Existing open source graphics API implementation ##
- http://glide.sourceforge.net/ - 3DFX Glide API